using System;

namespace Rick_and_Morty
{
    class Program
    {
        static void Main(string[] args)
        {
                        string your_actions = "Ваши действия:";
            Console.WriteLine("Добро пожаловать в квест по мотивам мультсериала Rick and Morty. Для управления используйте клавиши 1 и 2 на клавиатуре. Нажмите клавишу Enter, чтобы начать.");
            Console.ReadKey();
            Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
            Console.WriteLine("За окном ранее будничное утро.");
            Console.WriteLine("В комнату заходит дедушка Рик, и будит Вас с предложением полететь с ним в приключение.");
            Console.WriteLine(your_actions);
            Console.WriteLine("1. Остаться спать (-1 к рейтингу семьи)");
            Console.WriteLine("2. Пойти с дедушкой Риком (+1 к рейтингу семьи)");
            int choice1 = Convert.ToInt32(Console.ReadLine());
            if (choice1 == 1)
            {
                Morty.AddFamily(-1);
                Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                Console.WriteLine("Вы проснётесь по будильнику через 35 минут и пойдёте в школу");
                Console.WriteLine("У Вас урок математики, и Вы понимаете, что вы не сделали домашнюю работу и не готовы к контрольной работе, и внезапно появляется дедушка Рик.");
                Console.WriteLine(your_actions);
                Console.WriteLine("1. Остаться в школе и получить двойку (-1 к рейтингу учебы)");
                Console.WriteLine("2. Пойти с дедушкой Риком. (+1 к рейтингу семьи)");
                int choice = Convert.ToInt32(Console.ReadLine());
                if (choice == 1)
                {
                    Morty.AddStudy(-1);
                    Morty.AddFamily(-1);
                    Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                    Console.WriteLine("Придя домой, Ваша мама расстроится. (-1 к рейтингу семьи)");
                    Console.WriteLine("Вы идете в свою комнату учить уроки, чтобы исправить двойку.");
                    Console.WriteLine("Но вам приходит СМС от одноклассницы, которая Вам очень нравится, Вас приглашают на вечеринку.");
                    Console.WriteLine(your_actions);
                    Console.WriteLine("1. Пойти на вечеринку и не выучить уроки (+1 к рейтингу отношений, но -1 к рейтингу учебы)");
                    Console.WriteLine("2. Не пойти на вечеринку и выучить уроки (-1 к рейтингу отношений, но +1 к рейтингу учебы)");
                    int choice5 = Convert.ToInt32(Console.ReadLine());
                    if (choice5 == 1)
                    {
                        Morty.AddRelationships(1);
                        Morty.AddStudy(-1);
                        Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                        Console.WriteLine("На вечеринке Вам все понравилось, вы хорошо провели время");
                        Console.WriteLine("Придя домой, вы сразу ложитесь спать, утром вы понимаете, что не выспались, и понимаете, что не исправите двойку");
                        Console.WriteLine(your_actions);
                        Console.WriteLine("1. Пойти к дедушке Рику за помощью. (+1 к семье)");
                        Console.WriteLine("2. Пойти в школу (-1 к учебе)");
                        int choice6 = Convert.ToInt32(Console.ReadLine());
                        if (choice6 == 1)
                        {
                            Morty.AddFamily(1);
                            Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                        }
                        else if (choice6 == 2)
                        {
                            Morty.AddStudy(-1);
                            Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                        }

                    }
                    else if (choice5 == 2)
                    {
                        Morty.AddRelationships(-1);
                        Morty.AddStudy(1);
                        Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                        Console.WriteLine("Вы исправили двойку, и в школе вы получаете информацию, что на вечеринке было очень весело, и на вас обиделась одноклассница, которая вам нравится.");
                        Console.WriteLine(your_actions);
                        Console.WriteLine("1. Обратиться к дедушке Рику за помощью. (+1 к семье)");
                        int choice7 = Convert.ToInt32(Console.ReadLine());
                        if (choice7 == 1)
                        {
                            Morty.AddFamily(1);
                            Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                        }
                    }
                }
                else if (choice == 2)
                {
                    Morty.AddFamily(1);
                    Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                    Console.WriteLine("Вы пошли с дедушкой Риком и пропустили школу и полетите с ним на планету 'Юнити' (+1 к рейтингу семьи, но - 1 к рейтингу учебы)");
                    Morty.AddFamily(1);
                    Morty.AddStudy(-1);
                    Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                    Console.WriteLine("Вы хорошо провели время с дедушкой, но придя домой мама узнает, что Вы не были в школе. (-1 к рейтингу семьи)");
                    Morty.AddFamily(-1);
                    Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                    Console.WriteLine(your_actions);
                    Console.WriteLine("1. Свалить вину на дедушку (-1 к рейтингу семьи)");
                    Console.WriteLine("2. Попросить дедушку помочь (+1 к рейтингу семьи)");
                    int choice4 = Convert.ToInt32(Console.ReadLine());
                    if (choice4 == 1)
                    {
                        Morty.AddFamily(-1);
                        Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                        Console.WriteLine("Вы обидели дедушку Рика, и вы идете в свою комнату учить уроки, чтобы исправить двойку.");
                        Console.WriteLine("Но вам приходит СМС от одноклассницы, которая Вам очень нравится, Вас приглашают на вечеринку.");
                        Console.WriteLine(your_actions);
                        Console.WriteLine("1. Пойти на вечеринку и не выучить уроки (+1 к рейтингу отношений, но -1 к рейтингу учебы)");
                        Console.WriteLine("2. Не пойти на вечеринку и выучить уроки (-1 к рейтингу отношений, но +1 к рейтингу учебы)");
                        int choice8 = Convert.ToInt32(Console.ReadLine());
                        if (choice8 == 1)
                        {
                            Morty.AddRelationships(1);
                            Morty.AddStudy(-1);
                            Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                            Console.WriteLine("На вечеринке Вам все понравилось, вы хорошо провели время");
                            Console.WriteLine("Придя домой, вы сразу ложитесь спать, утром вы понимаете, что не выспались, и понимаете, что не исправите двойку");
                            Console.WriteLine(your_actions);
                            Console.WriteLine("1. Пойти к дедушке Рику за помощью. (+1 к семье)");
                            Console.WriteLine("2. Пойти в школу (-1 к учебе)");
                            int choice6 = Convert.ToInt32(Console.ReadLine());
                            if (choice6 == 1)
                            {
                                Morty.AddFamily(1);
                                Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                            }
                            else if (choice6 == 2)
                            {
                                Morty.AddStudy(-1);
                                Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                            }
                        }
                        else if (choice8 == 2)
                        {
                            Morty.AddRelationships(-1);
                            Morty.AddStudy(1);
                            Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                            Console.WriteLine("Вы исправили двойку, и в школе вы получаете информацию, что на вечеринке было очень весело, и на вас обиделась одноклассница, которая вам нравится.");
                            Console.WriteLine(your_actions);
                            Console.WriteLine("1. Обратиться к дедушке Рику за помощью. (+1 к семье)");
                            int choice7 = Convert.ToInt32(Console.ReadLine());
                            if (choice7 == 1)
                            {
                                Morty.AddFamily(1);
                                Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                            }
                        }
                    }
                    else if (choice4 == 2)
                    {
                        Morty.AddFamily(1);
                        Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                    }
                }
            }
            else if (choice1 == 2)
            {
                Morty.AddFamily(1);
                Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                Console.WriteLine("Вы пошли с дедушкой Риком и пропустили школу и полетите с ним на планету 'Юнити' (+1 к рейтингу семьи, но - 1 к рейтингу учебы)");
                Morty.AddFamily(1);
                Morty.AddStudy(-1);
                Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                Console.WriteLine("Вы хорошо провели время с дедушкой, но придя домой мама узнает, что Вы не были в школе. (-1 к рейтингу семьи)");
                Morty.AddFamily(-1);
                Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                Console.WriteLine(your_actions);
                Console.WriteLine("1. Свалить вину на дедушку (-1 к рейтингу семьи)");
                Console.WriteLine("2. Попросить дедушку помочь (+1 к рейтингу семьи)");
                int choice4 = Convert.ToInt32(Console.ReadLine());
                if (choice4 == 1)
                {
                    Morty.AddFamily(-1);
                    Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                    Console.WriteLine("Вы обидели дедушку Рика, и вы идете в свою комнату учить уроки, чтобы исправить двойку.");
                    Console.WriteLine("Но вам приходит СМС от одноклассницы, которая Вам очень нравится, Вас приглашают на вечеринку.");
                    Console.WriteLine(your_actions);
                    Console.WriteLine("1. Пойти на вечеринку и не выучить уроки (+1 к рейтингу отношений, но -1 к рейтингу учебы)");
                    Console.WriteLine("2. Не пойти на вечеринку и выучить уроки (-1 к рейтингу отношений, но +1 к рейтингу учебы)");
                    int choice8 = Convert.ToInt32(Console.ReadLine());
                    if (choice8 == 1)
                    {
                        Morty.AddRelationships(1);
                        Morty.AddStudy(-1);
                        Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                        Console.WriteLine("На вечеринке Вам все понравилось, вы хорошо провели время");
                        Console.WriteLine("Придя домой, вы сразу ложитесь спать, утром вы понимаете, что не выспались, и понимаете, что не исправите двойку");
                        Console.WriteLine(your_actions);
                        Console.WriteLine("1. Пойти к дедушке Рику за помощью. (+1 к семье)");
                        Console.WriteLine("2. Пойти в школу (-1 к учебе)");
                        int choice6 = Convert.ToInt32(Console.ReadLine());
                        if (choice6 == 1)
                        {
                            Morty.AddFamily(1);
                            Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                        }
                        else if (choice6 == 2)
                        {
                            Morty.AddStudy(-1);
                            Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                        }
                    }
                    else if (choice8 == 2)
                    {
                        Morty.AddRelationships(-1);
                        Morty.AddStudy(1);
                        Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                        Console.WriteLine("Вы исправили двойку, и в школе вы получаете информацию, что на вечеринке было очень весело, и на вас обиделась одноклассница, которая вам нравится.");
                        Console.WriteLine(your_actions);
                        Console.WriteLine("1. Обратиться к дедушке Рику за помощью. (+1 к семье)");
                        int choice7 = Convert.ToInt32(Console.ReadLine());
                        if (choice7 == 1)
                        {
                            Morty.AddFamily(1);
                            Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                        }
                    }
                }
                else if (choice4 == 2)
                {
                    Morty.AddRelationships(1);
                    Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
                }

            }
            Console.WriteLine("Вы идете с дедушкой Риком в гараж, и он изобретает пульт времени.");
            Console.WriteLine("Эпизод 2. Творение Рика.");
            Console.WriteLine("В гараже дедушка Рик делает пульт и исправляет все ошибки внука по имени " + Morty.name);
            Console.WriteLine("Родители Морти уезжают в командировку на один день.");
            Console.WriteLine("Морти, сестра Морти Саммер и дедушка Рик собирают своих друзей на вечеринке");
            Console.WriteLine(your_actions);
            Console.WriteLine("1. Позвать Джессику/Тоби (+1 к отношениям)");
            Console.WriteLine("2. Побояться позвать Джессику/Тоби(-1 к отношениям)");
            int choice2 = Convert.ToInt32(Console.ReadLine());
            if (choice2 == 1)
            {
                Morty.AddRelationships(1);
                Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
            }
            else if (choice2 == 2)
            {
                Morty.AddRelationships(-1);
                Console.WriteLine("Джессика узнает, что у вас была вечеринка и обидится на вас");
                Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
            }
            Console.WriteLine("На вечеринке происходит хаос, дом в полуразрушенном состоянии.");
            Console.WriteLine("И приезжают родители.");
            Console.WriteLine(your_actions);
            Console.WriteLine("1. Выслушать недоумевание родителей и приступить к уборке (-1 к семье)");
            Console.WriteLine("2. Воспользоваться пультом времени. (остановить время и прибраться) (+1 к семье)");
            choice2 = Convert.ToInt32(Console.ReadLine());
            if (choice2 == 1)
            {
                Morty.AddFamily(-1);
                Console.WriteLine("Родители сильно расстроятся, так как они доверились детям, а они их подвели. Морти, Саммер и Рик прибрали дом.");
                Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
            }
            else if (choice2 == 2)
            {
                Morty.AddFamily(1);
                Console.WriteLine("Время остановилось, Морти, сестра и Рик не спеша прибрались, но из-за пульта нельзя касаться друг друга, так как может произойти взрыв вселенной");
                Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
            }
            Console.WriteLine("На следующий день Морти приходит в школу, и при встрече Джессика/Тоби хочет Вас обнять.");
            Console.WriteLine(your_actions);
            Console.WriteLine("1. Обнять");
            Console.WriteLine("2. Воздержаться");
            choice2 = Convert.ToInt32(Console.ReadLine());
            if (choice2 == 1)
            {
                Morty.AddRelationships(1);
                Console.WriteLine("Вы договорились на свидание (+1 к отношениям)");
                Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
            }
            else if (choice2 == 2)
            {
                Console.WriteLine("Джессика/Тоби очень удивлен(а), но договорились на свидание");
                Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
            }
            Console.WriteLine(your_actions);
            Console.WriteLine("1. Попросить помощь у Рика");
            Console.WriteLine("2. Не прийти на свидание (-1 к отношениям)");
            choice2 = Convert.ToInt32(Console.ReadLine());
            if (choice2 == 1)
            {
                Console.WriteLine("Рик создает аппарат, который делает клонa Морти, чтобы клон пошел на свидание.");
                Console.WriteLine("Клона запрограммировали, обучили. Свидание проходит успешно.");
                Console.WriteLine("Клону Морти понравилась жизнь, но ему не нравится быть слугой, и он не хочет исчезать.");
                Console.WriteLine("Он украл космический корабль дедушки Рика и улетел на другую планету.");
                Console.WriteLine("У него появился злобный план - отомстить Рику. Так и появился Злой Морти.");
                Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
            }
            else if (choice2 == 2)
            {
                Morty.AddRelationships(-1);
                Console.WriteLine("Вы сообщаете Джессике/Тоби, что заболел(а).");
                Console.WriteLine($"Семья: {Morty.family}   Учёба: {Morty.study}   Отношения: {Morty.relationships}");
            }
            Console.WriteLine("                          The End");
            Console.WriteLine();
            Console.WriteLine("                           Титры");
            Console.WriteLine("В ролях:");
            Console.WriteLine();
            Console.WriteLine("        Морти");
            Console.WriteLine("        Дедушка Рик");
            Console.WriteLine("        Джессика/Тоби");
            Console.WriteLine("        Злой Морти");
            Console.WriteLine("        Саммер");
            Console.WriteLine();
            Console.WriteLine("Над квестом работали:");
            Console.WriteLine();
            Console.WriteLine("        Автор сценария    Илья Полегаев");
            Console.WriteLine();
            Console.WriteLine("        Программисты      Дмитрий Сизов");
            Console.WriteLine("                          Илья Полегаев");
            Console.WriteLine("                          Антонина Кожурина");

        }
    }
}
